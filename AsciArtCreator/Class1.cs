﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace AsciArtCreator
{
	class AsciCreator
	{
		private string imageLocation;
		private string pixels = ".:*|$JTLCYZVSUGPAOXFDE#RKHBQNWM";
		private int downscaleValueY = 4;
		private int downscaleValueX = 4;

		public AsciCreator(string imageLocation)
		{
			this.imageLocation = imageLocation;
		}

		public void Go(string saveLocation)
		{
			var image = new Bitmap(this.imageLocation);
		    image.MakeTransparent();

			using (StreamWriter sw = new StreamWriter(saveLocation, false, Encoding.UTF8))
			{
				sw.Write("<head><body><p1>");
				for (int y = 0; y < image.Height; y += this.downscaleValueY)
				{
					for (int x = 0; x < image.Width; x += this.downscaleValueX)
					{
						Color color = image.GetPixel(x, y);

						double brightness = AsciCreator.Brightness(color);

						double stringIndex = brightness / 255 * (pixels.Length - 1);

						char pixel = pixels[pixels.Length - (int)Math.Round(stringIndex) - 1];

						if (color.A != 255)
						{
                            sw.Write("<font size= 1 face= Courier New color = " + ColorTranslator.ToHtml(Color.White) + ">" + pixels[0] + "</font>");
                            sw.Write("<font size= 1 face= Courier New color = " + ColorTranslator.ToHtml(Color.White) + ">" + pixels[0] + "</font>");
						}
						else
						{
							sw.Write("<font size= 1 face= Courier New color = " + ColorTranslator.ToHtml(color) + ">" + pixel + "</font>");
							sw.Write("<font size= 1 face= Courier New color = " + ColorTranslator.ToHtml(color) + ">" + pixel + "</font>");
						}
					}
					
					sw.Write("<br>");
				}
				sw.Write("</p1></body></head>");
			}
		}

		private static double Brightness(Color c)
		{
			return Math.Sqrt(
				c.R * c.R * .241 +
				c.G * c.G * .691 +
				c.B * c.B * .068);
		}
	}
}
